package main

import (
	"log/slog"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/dewadg/turna/internal/app/client"
)

var rootCmd = &cobra.Command{
	Use:   os.Args[0],
	Short: "TurnA client CLI",
}

func init() {
	rootCmd.AddCommand(cmdDeploy())
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func cmdDeploy() *cobra.Command {
	var a client.Args

	cmd := &cobra.Command{
		Use:   "deploy",
		Short: "Works like `docker stack deploy`",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				slog.Error("missing stack name")
				os.Exit(1)
			}

			a.Stack = args[0]
			if err := client.Run(a); err != nil {
				os.Exit(1)
			}
		},
	}

	cmd.Flags().StringVarP(&a.ConfigPath, "compose-file", "c", "swarm.yml", "Swarm configuration file path")

	return cmd
}
