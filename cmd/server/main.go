package main

import (
	"os"

	"gitlab.com/dewadg/turna/internal/app/server"
)

func main() {
	if err := server.Run(); err != nil {
		os.Exit(1)
	}
}
