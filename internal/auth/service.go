package auth

import (
	"context"
	"errors"
	"log/slog"
	"time"

	"github.com/google/uuid"
	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Service struct {
	db             *gorm.DB
	accessTokenAge time.Duration
}

func NewService(db *gorm.DB, accessTokenAge time.Duration) *Service {
	return &Service{
		db:             db,
		accessTokenAge: accessTokenAge,
	}
}

var ErrUnauthorized = errors.New("unauthorized")

func (svc *Service) Authenticate(ctx context.Context, username, password string) (string, error) {
	var u entity.User
	err := svc.db.WithContext(ctx).Where("username = ?", username).First(&u).Error
	if err != nil {
		slog.Error("error getting user for auth", slog.Any("error", err))
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = ErrUnauthorized
		}
		return "", err
	}

	if err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)); err != nil {
		return "", ErrUnauthorized
	}

	at := entity.AccessToken{
		Token:     uuid.NewString(),
		UserID:    u.ID,
		ExpiredAt: time.Now().UTC().Add(svc.accessTokenAge),
	}
	if err = svc.db.WithContext(ctx).Create(&at).Error; err != nil {
		slog.Error("error storing access token", slog.Any("error", err))
		return "", err
	}

	return at.Token, nil
}

func (svc *Service) Authorize(ctx context.Context, token string) (entity.User, error) {
	var at entity.AccessToken
	err := svc.db.WithContext(ctx).Preload("User").Where("token = ?", token).First(&at).Error
	if err != nil {
		slog.Error("error getting access token", slog.Any("error", err))
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = ErrUnauthorized
		}
		return entity.User{}, err
	}

	if at.ExpiredAt.Before(time.Now().UTC()) {
		return entity.User{}, ErrUnauthorized
	}

	return at.User, nil
}
