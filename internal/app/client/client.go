package client

import (
	"bufio"
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"strings"
	"time"

	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"gopkg.in/yaml.v3"
)

type Args struct {
	Stack        string
	ConfigPath   string
	APIAddress   string
	ClientID     string
	ClientSecret string
}

func Run(a Args) error {
	slog.Info("deploying", slog.String("stack", a.Stack))

	deploymentID, err := run(a)
	if err != nil {
		slog.Error("error submitting deployment", slog.String("stack", a.Stack), slog.Any("error", err))
		return err
	}

	slog.Info("deployment submitted successfully",
		slog.String("stack", a.Stack),
		slog.String("deployment_id", deploymentID),
	)

	return nil
}

type submitDeploymentResponse struct {
	Error string            `json:"error"`
	Data  entity.Deployment `json:"data"`
}

func run(a Args) (string, error) {
	if a.Stack == "" {
		return "", errors.New("stack name is required")
	}
	if a.ConfigPath == "" {
		return "", errors.New("config file is required")
	}
	if a.APIAddress == "" {
		a.APIAddress = os.Getenv("TURNA_ADDRESS")
	}
	if a.ClientID == "" {
		a.ClientID = os.Getenv("TURNA_CLIENT_ID")
	}
	if a.ClientSecret == "" {
		a.ClientSecret = os.Getenv("TURNA_CLIENT_SECRET")
	}

	closers := make([]io.Closer, 0)
	defer func() {
		for _, closer := range closers {
			_ = closer.Close()
		}
	}()

	cf, err := os.Open(a.ConfigPath)
	if err != nil {
		return "", err
	}
	closers = append(closers, cf)

	var swarmCfg entity.DockerComposeFile
	if err = yaml.NewDecoder(cf).Decode(&swarmCfg); err != nil {
		return "", err
	}

	for svcName, svc := range swarmCfg.Services {
		if len(svc.EnvFile) == 0 {
			continue
		}
		if len(svc.Env) == 0 {
			svc.Env = make([]string, 0)
		}

		for _, envFile := range svc.EnvFile {
			envFileF, err := os.Open(envFile)
			if err != nil {
				return "", fmt.Errorf("failed to open env file for service %s: %w", svcName, err)
			}
			closers = append(closers, envFileF)

			r := bufio.NewReader(envFileF)
			for {
				val, err := r.ReadString('\n')
				if err != nil {
					if errors.Is(err, io.EOF) {
						break
					}
					return "", err
				}

				svc.Env = append(svc.Env, strings.TrimSuffix(val, "\n"))
			}
		}

		swarmCfg.Services[svcName] = svc
	}

	buf := bytes.NewBuffer(nil)
	err = json.NewEncoder(buf).Encode(entity.Deployment{
		Stack:  a.Stack,
		Config: swarmCfg,
	})
	if err != nil {
		return "", err
	}

	hasher := hmac.New(sha256.New, []byte(a.ClientSecret))
	hasher.Write(buf.Bytes())

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, a.APIAddress+"/api/deployments", buf)
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Turna-Signature", fmt.Sprintf("%x", hasher.Sum(nil)))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var respBody submitDeploymentResponse
	if err = json.NewDecoder(resp.Body).Decode(&respBody); err != nil {
		return "", err
	}
	if resp.StatusCode != http.StatusAccepted {
		return "", fmt.Errorf("unexpected status code: %d - %s", resp.StatusCode, respBody.Error)
	}

	return respBody.Data.ID, nil
}
