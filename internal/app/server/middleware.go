package server

import (
	"context"
	"errors"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/dewadg/turna/internal/auth"
	"gitlab.com/dewadg/turna/internal/pkg/contextutil"
)

func authMiddleware(authSvc *auth.Service) fiber.Handler {
	return func(c *fiber.Ctx) error {
		token := strings.TrimPrefix(c.Get("Authorization"), "Bearer ")
		if len(token) == 0 {
			return c.Status(fiber.StatusUnauthorized).JSON(errorResponse{
				Error: auth.ErrUnauthorized.Error(),
			})
		}

		ctx := c.UserContext()
		user, err := authSvc.Authorize(ctx, token)
		if err != nil {
			c = c.Status(fiber.StatusInternalServerError)
			if errors.Is(err, auth.ErrUnauthorized) {
				c = c.Status(fiber.StatusUnauthorized)
			}
			return c.JSON(errorResponse{
				Error: err.Error(),
			})
		}

		ctx = context.WithValue(ctx, contextutil.KeyUserID, user)
		c.SetUserContext(ctx)

		return c.Next()
	}
}
