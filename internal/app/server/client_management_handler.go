package server

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dewadg/turna/internal/clientmanagement"
)

type createClientRequest struct {
	Description string `json:"description"`
}

func handleCreateClient(clientManSvc *clientmanagement.Service) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var reqBody createClientRequest
		if err := c.BodyParser(&reqBody); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		ctx := c.UserContext()
		newClient, err := clientManSvc.Create(ctx, reqBody.Description)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		return c.Status(fiber.StatusCreated).JSON(response{
			Message: "client created successfully",
			Data:    newClient,
		})
	}
}

func handleGetClients(clientManSvc *clientmanagement.Service) fiber.Handler {
	return func(c *fiber.Ctx) error {
		ctx := c.UserContext()
		clients, err := clientManSvc.Get(ctx)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		return c.JSON(response{
			Message: "clients retrieved successfully",
			Data:    clients,
		})
	}
}
