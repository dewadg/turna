package server

type errorResponse struct {
	Error string `json:"error"`
}

type response struct {
	Message string `json:"message"`
	Data    any    `json:"data"`
}
