package server

import (
	"errors"
	"gitlab.com/dewadg/turna/internal/pkg/contextutil"
	"gitlab.com/dewadg/turna/internal/pkg/entity"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/dewadg/turna/internal/auth"
)

type authenticationRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func handleAuthenticate(authSvc *auth.Service) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var reqBody authenticationRequest
		if err := c.BodyParser(&reqBody); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		ctx := c.UserContext()
		accessToken, err := authSvc.Authenticate(ctx, reqBody.Username, reqBody.Password)
		if err != nil {
			c = c.Status(fiber.StatusInternalServerError)
			if errors.Is(err, auth.ErrUnauthorized) {
				c = c.Status(fiber.StatusBadRequest)
			}

			return c.JSON(errorResponse{
				Error: err.Error(),
			})
		}

		return c.JSON(response{
			Message: "authentication successful",
			Data:    accessToken,
		})
	}
}

func handleWhoami() fiber.Handler {
	return func(c *fiber.Ctx) error {
		user, _ := c.UserContext().Value(contextutil.KeyUserID).(entity.User)

		return c.JSON(response{
			Message: "you are",
			Data:    user,
		})
	}
}
