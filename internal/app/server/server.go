package server

import (
	"log/slog"

	"github.com/docker/docker/client"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/dewadg/turna/internal/auth"
	"gitlab.com/dewadg/turna/internal/clientmanagement"
	"gitlab.com/dewadg/turna/internal/deployment"
	"gitlab.com/dewadg/turna/internal/pkg/config"
	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"gitlab.com/dewadg/turna/internal/pkg/seeder"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	cfg       *config.Config
	dockerCli *client.Client
	db        *gorm.DB
	app       *fiber.App
)

func Run() error {
	var err error

	cfg, err = config.Load()
	if err != nil {
		slog.Error("error loading config", slog.Any("error", err))
		return err
	}

	dockerCli, err = client.NewClientWithOpts(client.WithHost(cfg.DockerSocketPath), client.WithAPIVersionNegotiation())
	if err != nil {
		slog.Error("error creating docker client", slog.Any("error", err))
		return err
	}

	db, err = gorm.Open(sqlite.Open(cfg.SqliteDSN))
	if err != nil {
		slog.Error("error connecting to database", slog.Any("error", err))
		return err
	}
	if err = entity.Migrate(db); err != nil {
		slog.Error("error running migrations", slog.Any("error", err))
		return err
	}
	if err = seeder.Seed(db); err != nil {
		slog.Error("error running seeder", slog.Any("error", err))
		return err
	}

	app = fiber.New()
	if err = setup(); err != nil {
		slog.Error("error setting up app", slog.Any("error", err))
		return err
	}

	addr := "127.0.0.1:8000"
	if cfg.AppEnv == "production" {
		addr = "0.0.0.0:8000"
	}

	if err = app.Listen(addr); err != nil {
		slog.Error("http server error", slog.Any("error", err))
		return err
	}

	return nil
}

func setup() error {
	app.Use(cors.New())

	app.Get("/", func(c *fiber.Ctx) error {
		return c.JSON("Hello, World")
	})

	apiRoutes := app.Group("/api")

	authSvc := auth.NewService(db, cfg.AccessTokenAge)
	authMdlw := authMiddleware(authSvc)

	apiRoutes.Post("/auth/login", handleAuthenticate(authSvc))
	apiRoutes.Get("/auth/whoami", authMdlw, handleWhoami())

	clientManSvc := clientmanagement.NewService(db)

	apiRoutes.Post("/clients", authMdlw, handleCreateClient(clientManSvc))
	apiRoutes.Get("/clients", authMdlw, handleGetClients(clientManSvc))

	deploymentSvc := deployment.NewService(db, dockerCli)

	apiRoutes.Post("/deployments", handleSubmitDeployment(deploymentSvc))

	return nil
}
