package server

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dewadg/turna/internal/deployment"
	"gitlab.com/dewadg/turna/internal/pkg/entity"
)

type SubmitDeploymentRequest struct {
	entity.Deployment
}

func handleSubmitDeployment(deploymentSvc *deployment.Service) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var req SubmitDeploymentRequest
		if err := c.BodyParser(&req); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		ctx := c.UserContext()
		d, err := deploymentSvc.Deploy(ctx, req.Deployment)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(errorResponse{
				Error: err.Error(),
			})
		}

		return c.Status(fiber.StatusAccepted).JSON(response{
			Message: "deployment submitted",
			Data:    d,
		})
	}
}
