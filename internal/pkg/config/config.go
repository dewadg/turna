package config

import (
	"time"

	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
)

type Config struct {
	AppEnv           string        `env:"APP_ENV" envDefault:"development"`
	SqliteDSN        string        `env:"SQLITE_DSN"`
	AccessTokenAge   time.Duration `env:"ACCESS_TOKEN_AGE" envDefault:"1h"`
	DockerSocketPath string        `env:"DOCKER_SOCKET_PATH"`
}

func Load() (*Config, error) {
	_ = godotenv.Load()
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, err
	}
	return &cfg, nil
}
