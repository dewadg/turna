package entity

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) error {
	return db.AutoMigrate(
		&SeederHistory{},
		&User{},
		&AccessToken{},
		&Client{},
		&Deployment{},
	)
}

type SeederHistory struct {
	Description string
	ExecutedAt  time.Time
}

type User struct {
	ID        uint       `gorm:"primaryKey" json:"id"`
	Username  string     `json:"username"`
	Password  string     `json:"-"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `gorm:"index" json:"-"`
}

type AccessToken struct {
	Token     string `gorm:"primaryKey"`
	UserID    uint
	ExpiredAt time.Time
	User      User
}

type Client struct {
	ID          string     `gorm:"primaryKey" json:"id"`
	Description string     `json:"description"`
	Secret      string     `json:"-"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	RevokedAt   *time.Time `gorm:"index" json:"revokedAt"`
}

const (
	DeploymentStatusPending   = "pending"
	DeploymentStatusDeploying = "deploying"
	DeploymentStatusSuccess   = "success"
	DeploymentStatusFailed    = "failed"
)

type Deployment struct {
	ID          string            `json:"id"`
	Status      string            `json:"status"`
	Stack       string            `json:"stack"`
	Config      DockerComposeFile `gorm:"type:text" json:"config"`
	CreatedAt   time.Time         `json:"createdAt"`
	CompletedAt *time.Time        `json:"completedAt"`
}

type DockerComposeFile struct {
	Version  string                   `yaml:"version" json:"version"`
	Services map[string]DockerService `yaml:"services" json:"services"`
	Networks map[string]DockerNetwork `yaml:"networks,omitempty" json:"networks,omitempty"`
	Volumes  map[string]DockerVolume  `yaml:"volumes,omitempty" json:"volumes,omitempty"`
}

type DockerService struct {
	Image       string            `yaml:"image,omitempty" json:"image,omitempty"`
	Ports       []string          `yaml:"ports,omitempty" json:"ports,omitempty"`
	Deploy      DockerDeploy      `yaml:"deploy,omitempty" json:"deploy,omitempty"`
	Environment map[string]string `yaml:"environment,omitempty" json:"environment,omitempty"`
	EnvFile     []string          `yaml:"env_file,omitempty" json:"env_file,omitempty"`
	Env         []string          `yaml:"env,omitempty" json:"env,omitempty"`
	Volumes     []string          `yaml:"volumes,omitempty" json:"volumes,omitempty"`
	Networks    []string          `yaml:"networks,omitempty" json:"networks,omitempty"`
}

type DockerDeploy struct {
	Replicas      int                 `yaml:"replicas,omitempty" json:"replicas,omitempty"`
	RestartPolicy DockerRestartPolicy `yaml:"restart_policy,omitempty" json:"restart_policy,omitempty"`
	Placement     DockerPlacement     `yaml:"placement,omitempty" json:"placement,omitempty"`
	Labels        []string            `yaml:"labels,omitempty" json:"labels,omitempty"`
}

type DockerRestartPolicy struct {
	Condition string `yaml:"condition,omitempty" json:"condition,omitempty"`
}

type DockerPlacement struct {
	Constraints []string `yaml:"constraints,omitempty" json:"constraints,omitempty"`
}

type DockerNetwork struct {
	Driver     string            `yaml:"driver,omitempty" json:"driver,omitempty"`
	DriverOpts map[string]string `yaml:"driver_opts,omitempty" json:"driver_opts,omitempty"`
}

type DockerVolume struct {
	Driver     string            `yaml:"driver,omitempty" json:"driver,omitempty"`
	DriverOpts map[string]string `yaml:"driver_opts,omitempty" json:"driver_opts,omitempty"`
}

func (j *DockerComposeFile) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New("failed to unmarshal DeploymentConfig value")
	}

	var result DockerComposeFile
	err := json.Unmarshal(bytes, &result)
	*j = result
	return err
}

func (j DockerComposeFile) Value() (driver.Value, error) {
	return json.Marshal(j)
}
