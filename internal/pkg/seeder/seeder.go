package seeder

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"gorm.io/gorm"
)

type Seeder interface {
	Description() string

	Run(context.Context, *gorm.DB) error
}

func getSeeders() []Seeder {
	return []Seeder{
		seedUsersTable{},
	}
}

func Seed(db *gorm.DB) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	var shs []entity.SeederHistory
	if err := db.WithContext(ctx).Find(&shs).Error; err != nil {
		return err
	}

	executed := make(map[string]struct{}, len(shs))
	for _, sh := range shs {
		executed[sh.Description] = struct{}{}
	}

	for _, seeder := range getSeeders() {
		if _, alreadyExecuted := executed[seeder.Description()]; alreadyExecuted {
			continue
		}

		sh := entity.SeederHistory{
			Description: seeder.Description(),
			ExecutedAt:  time.Now().UTC(),
		}
		if err := seeder.Run(ctx, db); err != nil {
			return fmt.Errorf("error seeder %s: %w", seeder.Description(), err)
		}
		if err := db.WithContext(ctx).Create(&sh).Error; err != nil {
			return err
		}
	}

	return nil
}
