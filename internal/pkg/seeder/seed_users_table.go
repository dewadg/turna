package seeder

import (
	"context"

	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type seedUsersTable struct{}

func (s seedUsersTable) Description() string {
	return "seed_users_table"
}

func (s seedUsersTable) Run(ctx context.Context, db *gorm.DB) error {
	u := entity.User{
		Username: "admin",
		Password: "admin",
	}
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(passwordHash)

	return db.WithContext(ctx).Create(&u).Error
}
