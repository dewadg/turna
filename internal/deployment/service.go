package deployment

import (
	"context"
	"fmt"
	"log/slog"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/oklog/ulid/v2"
	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"gorm.io/gorm"
)

type Service struct {
	db        *gorm.DB
	dockerCli *client.Client
}

func NewService(db *gorm.DB, dockerCli *client.Client) *Service {
	return &Service{
		db:        db,
		dockerCli: dockerCli,
	}
}

func (svc *Service) Deploy(ctx context.Context, d entity.Deployment) (entity.Deployment, error) {
	d.ID = ulid.Make().String()
	d.Status = entity.DeploymentStatusPending
	d.CreatedAt = time.Now().UTC()
	err := svc.db.WithContext(ctx).Create(&d).Error
	if err != nil {
		return entity.Deployment{}, err
	}

	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
		defer cancel()

		if err := svc.handleDeployment(ctx, d); err != nil {
			slog.Error("error performing deployment", slog.Any("error", err))
		}
	}()

	return d, nil
}

func (svc *Service) handleDeployment(ctx context.Context, d entity.Deployment) error {
	d.Status = entity.DeploymentStatusDeploying
	if err := svc.db.WithContext(ctx).Save(&d).Error; err != nil {
		return err
	}

	for svcName, svcSpec := range d.Config.Services {
		portSpecs := make([]swarm.PortConfig, 0, len(svcSpec.Ports))
		for i, portSpec := range svcSpec.Ports {
			ports := strings.Split(portSpec, ":")
			if len(ports) != 2 {
				continue
			}

			targetPort, err := strconv.Atoi(ports[1])
			if err != nil {
				return fmt.Errorf("invalid target port %s at index %d on service %s", ports[1], i, svcName)
			}

			publishedPort, err := strconv.Atoi(ports[0])
			if err != nil {
				return fmt.Errorf("invalid published port %s at index %d on service %s", ports[1], i, svcName)
			}

			portSpecs = append(portSpecs, swarm.PortConfig{
				Protocol:      swarm.PortConfigProtocolTCP,
				TargetPort:    uint32(targetPort),
				PublishedPort: uint32(publishedPort),
			})
		}

		labelsSpec := make(map[string]string, len(svcSpec.Deploy.Labels))
		for _, label := range svcSpec.Deploy.Labels {
			labelSplit := strings.Split(label, "=")
			if len(labelSplit) != 2 {
				continue
			}
			labelsSpec[labelSplit[0]] = labelSplit[1]
		}

		spec := swarm.ServiceSpec{
			Annotations: swarm.Annotations{
				Name:   d.Stack + "--" + svcName,
				Labels: labelsSpec,
			},
			TaskTemplate: swarm.TaskSpec{
				ContainerSpec: &swarm.ContainerSpec{
					Image: svcSpec.Image,
					Env:   svcSpec.Env,
				},
				RestartPolicy: &swarm.RestartPolicy{
					Condition: swarm.RestartPolicyCondition(svcSpec.Deploy.RestartPolicy.Condition),
				},
			},
			EndpointSpec: &swarm.EndpointSpec{
				Ports: portSpecs,
			},
			Mode: swarm.ServiceMode{
				Replicated: &swarm.ReplicatedService{
					Replicas: &[]uint64{uint64(svcSpec.Deploy.Replicas)}[0],
				},
			},
		}

		serviceID, serviceVersion, err := svc.checkService(ctx, spec.Annotations.Name)
		if err != nil {
			return err
		}

		if serviceID == "" {
			_, err = svc.dockerCli.ServiceCreate(ctx, spec, types.ServiceCreateOptions{
				QueryRegistry: true,
			})
			if err != nil {
				return err
			}
		} else {
			_, err = svc.dockerCli.ServiceUpdate(ctx, serviceID, serviceVersion, spec, types.ServiceUpdateOptions{
				QueryRegistry: true,
			})
		}
	}

	d.Status = entity.DeploymentStatusSuccess
	d.CompletedAt = &[]time.Time{time.Now().UTC()}[0]
	if err := svc.db.WithContext(ctx).Save(&d).Error; err != nil {
		return err
	}

	return nil
}

func (svc *Service) checkService(ctx context.Context, name string) (string, swarm.Version, error) {
	f := filters.NewArgs()
	f.Add("name", name)

	svcs, err := svc.dockerCli.ServiceList(ctx, types.ServiceListOptions{
		Filters: f,
	})
	if err != nil {
		return "", swarm.Version{}, err
	}
	if len(svcs) == 0 {
		return "", swarm.Version{}, nil
	}

	return svcs[0].ID, svcs[0].Version, nil
}
