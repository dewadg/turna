package clientmanagement

import (
	"context"
	"log/slog"

	"github.com/google/uuid"
	"gitlab.com/dewadg/turna/internal/pkg/entity"
	"gorm.io/gorm"
)

type Service struct {
	db *gorm.DB
}

func NewService(db *gorm.DB) *Service {
	return &Service{
		db: db,
	}
}

func (svc *Service) Create(ctx context.Context, description string) (entity.Client, error) {
	c := entity.Client{
		ID:          uuid.NewString(),
		Secret:      uuid.NewString(),
		Description: description,
	}
	err := svc.db.WithContext(ctx).Create(&c).Error
	if err != nil {
		slog.Error("error creating client", slog.Any("error", err))
		return entity.Client{}, err
	}
	return c, nil
}

func (svc *Service) Get(ctx context.Context) ([]entity.Client, error) {
	var clients []entity.Client
	err := svc.db.WithContext(ctx).Find(&clients).Error
	if err != nil {
		slog.Error("error getting fetching clients", slog.Any("error", err))
		return nil, err
	}
	return clients, nil
}
