import { createApp } from 'vue'
import { createPinia } from 'pinia'
import PrimeVue from 'primevue/config'
import 'primeflex/primeflex.css'
import 'primevue/resources/themes/aura-light-blue/theme.css'
import 'primeicons/primeicons.css'
import './assets/main.css'
import App from './App.vue'
import { router } from './router'

const app = createApp(App)

app.use(router)
app.use(PrimeVue)
app.use(createPinia())
app.mount('#app')
