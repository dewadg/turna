import { reactive, ref } from 'vue'
import { defineStore } from 'pinia'
import { API_URL } from './config.js'

export const useAuthStore = defineStore('auth', () => {
  const loading = ref(false)

  const currentUser = reactive({
    id: undefined,
    username: ''
  })

  async function authenticate({ username, password }) {
    const resp = await fetch(`${API_URL}/auth/login`, {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: {
        'Content-Type': 'application/json'
      }
    })

    const data = await resp.json()
    const accessToken = data.data

    window.localStorage.setItem('turnaAccessToken', accessToken)
  }

  async function whoami() {
    const accessToken = window.localStorage.getItem('turnaAccessToken') || ''
    if (!accessToken) {
      throw new Error('Unauthenticated')
    }

    const resp = await fetch(`${API_URL}/auth/whoami`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })

    const data = await resp.json()

    currentUser.id = data.data.id
    currentUser.username = data.data.username
  }

  function logout() {
    window.localStorage.removeItem('turnaAccessToken')
  }

  return {
    loading,
    authenticate,
    whoami,
    currentUser,
    logout
  }
})
